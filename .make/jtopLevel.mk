#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Top level makefile for C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		APPLI 			Application (program or jar) name
#		COPYRIGHT 		Year(s) for copyright clause
#------------------------------------------------------------------------------
MUTE			= @
OPTIM_DEBUG		= -O0 -g
OPTIM_RELEASE	= -O2
STRIP_DEBUG		= :
STRIP_RELEASE	= strip
OPTIM 			= "$(OPTIM_DEBUG)"
STRIP 			= $(STRIP_DEBUG)
#-------------------------------------------------------------------------------
VERSION			= $(shell cat VERSION)
TAG 			= "Version $(VERSION) - $(shell LC_ALL=en_EN.UTF-8 LANG=en_EN.utf8 date '+%B %Y')"
GIT 			= git
#-------------------------------------------------------------------------------
include .make/help.mk
#-------------------------------------------------------------------------------
all:
	@echo "------------------------------------------------------------ $(APPLI)_c"
	$(MUTE)cd  $(APPLI)_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION=$(VERSION) COPYRIGHT=$(COPYRIGHT) OPTIM=$(OPTIM) STRIP=$(STRIP); cd ..
	@echo "------------------------------------------------------------ $(APPLI)_java"
	$(MUTE)cd  $(APPLI)_java; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION=$(VERSION); cd ..
clean:
	@echo "------------------------------------------------------------ $(APPLI)_c"
	$(MUTE)cd  $(APPLI)_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
	@echo "------------------------------------------------------------ $(APPLI)_java"
	$(MUTE)cd  $(APPLI)_java; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
	@echo "------------------------------------------------------------ $(APPLI)_bin"
	$(MUTE)cd  $(APPLI)_bin; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
astyle cppcheck:
	@echo "------------------------------------------------------------ $(APPLI)_c"
	$(MUTE)cd  $(APPLI)_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
install cleaninstall:
	@echo "------------------------------------------------------------ $(APPLI)_c"
	$(MUTE)cd $(APPLI)_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
	@echo "------------------------------------------------------------ $(APPLI)_bin"
	$(MUTE)cd $(APPLI)_bin; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
release:
	$(MUTE)$(MAKE) --no-print-directory clean
	$(MUTE)$(MAKE) --no-print-directory all OPTIM="$(OPTIM_RELEASE)" STRIP=$(STRIP_RELEASE)
	$(MUTE)$(MAKE) --no-print-directory cleaninstall
	$(MUTE)$(MAKE) --no-print-directory install
	$(MUTE)$(MAKE) --no-print-directory clean
commit:
	$(GIT) commit -m $(TAG)
	$(GIT) tag -a v$(VERSION) -m $(TAG)
	$(GIT) push
	$(GIT) push --tags
#-------------------------------------------------------------------------------
.PHONY: all clean install cleaninstall astyle cppcheck commit release help
#-------------------------------------------------------------------------------
