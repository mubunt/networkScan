
# APPLICATION: *networkScan*

 **networkScan**  is a very simple application allowing to graphically visualize  the peripherals (computers, phones, tablets, plugs, etc.) connected to his local network. This application has been developed strictly for a  personal / family environment and therefore has nothing to do with sophisticated professional or semi-professional applications like *SoftPerfect Network Scanner*, *Advanced IP Scanner*, or the excellent *Angry IP Scanner*.
 
 **Example:**
 ![netwokScan interface](README_images/networkScan-00.png  "netwokScan interface 1")
 
The local network is represented either as a rake or as a circle. Each connected device is represented by its IP address. The network is periodically scanned  and the graph is updated accordingly. A new connection is indicated in green; This information will be re-written in black when this connection changes from the "new" state to the "already connected" state.

**Example:**
![netwokScan interface](README_images/networkScan-01.png  "netwokScan interface 2")

![netwokScan interface](README_images/networkScan-C01.png  "netwokScan interface 3")

If the information for each connection is not fully displayed  (longer than the display area), it is also available in the associated tooltip.

**Example:**
![netwokScan interface](README_images/networkScan-06.png  "netwokScan interface 4")

![netwokScan interface](README_images/networkScan-C06.png  "netwokScan interface 5")

The user can associate with the application a directory file listing the physical addresses of the devices that he knows, as well as a short description. This directory also allows to detect unknown devices and to signal them by displaying a "warning" icon in front of their IP address:
![Warning icon](README_images/networkScan-05.png  "Warning icon")


The directory file contains a set of lines, one line per device. Each line consists of 3 fields separated by a tab:
- Physical hardware address (mandatory)
- One line escription (optional) 
- Path to a '.wav' sound file (optional). This path is either absolute or relative to the "*networkScan_bin*" directory under the installation directory.

The sound file is played as soon as the device is detected (Not very important but fun for short repetitive connections).

**Example:**

	f4:ca:e5:49:0d:2c	FreeBox (GateWay & DNS server)
	00:24:d4:71:72:a4	FreeBox-Player
	68:a3:78:2d:46:47	FreeBox-Mini
	d8:eb:97:1b:ea:8d	PC-Family (Christiane)
	84:63:d6:12:c5:c0	PC-White LapTop
	14:da:e9:f7:25:c1	michel-ubuntu    Sounds/hallelujah.wav
	20:e5:64:f0:0f:14	LivePlug
	20:e5:64:f0:0d:ae	LivePlug
	a0:1d:48:ed:01:d0	Printer/Scanner Photosmart 6520 series  Sounds/cougar.wav

The link between the connection IP address and the directory file is of course the physical hardware address.

Two buttons complete the **networkScan** interface.
1. The "*Force*" button allows the user to launch a network scan immediately, without waiting for the next scanning period.
2. The "*Report*" button displays in text form the information presented in the main window.

**Example:**
![netwokScan interface](README_images/networkScan-03.png  "netwokScan interface")

## LICENSE
**networkScan** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE 
``` Bash
$ networkScan --help
networkScan - Copyright (c) , Michel RIZZO. All Rights Reserved.
networkScan - Version 1.9.2

A local network scanner

Usage: networkScan [OPTIONS]... [FILES]...

  -h, --help            Print help and exit
  -V, --version         Print version and exit
  -v, --verbose         Verbose mode.  (default=off)
  -C, --circle          Representation of the network as a circle.
                          (default=off)
  -d, --directory=FILE  Physical hardware address directory. Default is
                          'HWAddress.directory'.
  -D, --delay=INTEGER   Delay in seconds between 2 scans. Default is 60
                          seconds.
$ 
```

## STRUCTURE OF THE APPLICATION
This section walks you through **networkScan**'s structure. Once you understand this structure, you will easily find your way around in **networkScan**'s code base.

``` Bash
$ yaTree
./                                    # Application level
├── README_images/                    # Images for documentation
│   ├── networkScan-00.png            # 
│   ├── networkScan-01.png            # 
│   ├── networkScan-02.png            # 
│   ├── networkScan-03.png            # 
│   ├── networkScan-05.png            # 
│   ├── networkScan-06.png            # 
│   ├── networkScan-C01.png           # 
│   ├── networkScan-C06.png           # 
│   └── networkScan.png               # 
├── networkScan_bin/                  # Binary directory: jars (third-parties and local) and driver
│   ├── sounds/                       # -- Sound files examples
│   │   ├── cougar.wav                # 
│   │   ├── dedodedo.wav              # 
│   │   └── hallelujah.wav            # 
│   ├── HWAddress.directory           # -- Example of Hardware Address directory file
│   ├── Makefile                      # -- Makefile
│   ├── commons-cli-1.4.jar           # -- Third-party COMMONS CLI 1.4 jar file
│   └── swt-linux-415.jar             # -- Third-party SWT 1.4 jar file
├── networkScan_c/                    # C Source directory
│   ├── Makefile                      # -- Makefile
│   ├── networkScan.c                 # -- C main source file
│   └── networkScan.ggo               # -- 'gengetopt' option definition
│                                     # -- Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── networkScan_java/                 # JAVA Source directory
│   ├── networkScan/                  # -- IntelliJ infoVault project structure
│   │   ├── src/                      # 
│   │   │   ├── deviceDirectory.java  # 
│   │   │   ├── networkBuildInfo.java # 
│   │   │   ├── networkCmd.java       # 
│   │   │   ├── networkGui.java       # 
│   │   │   ├── networkReport.java    # 
│   │   │   └── networkScan.java      # 
│   │   └── networkScan.iml           # 
│   └── Makefile                      # -- Makefile
├── COPYING.md                        # GNU General Public License markdown file
├── LICENSE.md                        # License markdown file
├── Makefile                          # Top-level makefile
├── README.md                         # ReadMe Mark-Down file
├── RELEASENOTES.md                   # Release Notes markdown file
└── VERSION                           # Version identification text file

7 directories, 33 files
$
```

## HOW TO BUILD THIS APPLICATION
``` Bash
	$ cd networkScan
	$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
``` Bash
	$ cd networkScan
	$ make release
		# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    	# We consider that $BIN_DIR is a part of the PATH.
	$ networkScan <options>
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *networkScan/networkScan_java/networkScan* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
  - JAVA 1.8.0 for usage and development
  - Command *nmap*:  Network exploration tool and security / port scanner.
    - Command installed as standard on Linux.
  - Command *arp*: address resolution display and control
    - Command installed as standard on Linux.
  - Command *ping*: send ICMP ECHO_REQUEST to network hosts
    - Command installed as standard on Linux.
  - Commande *route*.
- For development:
 - IntelliJ IDEA 2019.2.4 (Community Edition) Build #IC-192.7142.36, built on October 29, 2019
  - Openjdk:
    - version "1.8.0_232-ea"
    - OpenJDK Runtime Environment (build 1.8.0_232-ea-8u232-b09-0ubuntu1-b09)
    - OpenJDK 64-Bit Server VM (build 25.232-b09, mixed mode)
  - GNU gengetopt 2.22.6 

Application developed and tested with XUBUNTU 18.04.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES) .

***
