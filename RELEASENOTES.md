# RELEASE NOTES: *networkScan*, a local network scanner

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.9.7**:
  - Updated build system components.

- **Version 1.9.6**:
  - Updated build system.

- **Version 1.9.5**:
  - Removed unused files.

- **Version 1.9.4**:
  - Updated build system component(s)

- **Version 1.9.3**:
  - Renamed "Sounds" directory as "sounds" (to be compliant with destination directory).
  - Fixed readme content.
  - Specified font color for gataway box (difference between swt version).

- **Version 1.9.2**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.9.1**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 1.9.0**:
  - Removed build method for Windows and associated files.

- **Version 1.8.2**:
  - Some minor changes in project structure and build

- **Version 1.8.1**:
  - Removed obsolete option (--jar) in help.

- **Version 1.8.0**
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).
  - Moved from GPL v2 to GPL v3.
  - Moved to SWT 4.12. Initially was 4.6.3.
  - Improved ./Makefile, ./networkScan_bin/Makefile, ./networkScan_c/Makefile, ./networkScan_java/Makefile.
  - Removed C compilation warnings.
  - Updated README file.

- **Version 1.7.0**:
  - Added ".comment" file in each directory for 'yaTree' utility.
-
 **Version 1.6.1**:
  - Fixed typos and omission in README file.

- **Version 1.6.0**:
  - Driver is now a C executable. Was a script (bash / cmd) file.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  - Move to SWT 4.6.3. Initially was 4.6.2.
  - Specified foreground and background colors after migration from Ubuntu/Mate
	  to XUbuntu/Xfce4 (slightly differents with both 2 window managers).

- **Version 1.5.0**:
  - Deleted the "local" and "public" options; i fact, it is impossible to obtain,
	  in Java, the MAC address of a device connected to the public network. Only the
	  local network is now scanned.
  - Added the representation of the network as a rake. Circle representation is
	  kept with the new "--circle | -C" option.

- **Version 1.4.0**:
  - Replaced "--address" parameter by "--local" and "'"--public" options.

- **Version 1.3.2 - Build 27**:
  - Fixed a defect in the processing of the "--address" parameter.

- **Version 1.3.2 - Build 26**:
  - Moved to SWT (Standard Widget Toolkit) 4.6.2 (previous: 4.6.1).

- **Version 1.3.1 - Build 26**:
  - The window is now resizable.

- **Version 1.3.0 - Build 25**:
  - Implemented Wndows version.

- **Version 1.2.1 - Build 21**:
  - Set by-default hardware directory file as *installdir*/networkScan_bin/HWAddress.directory.
-
 **Version 1.2.0 - Build 20**:
  - Display all the information in the initial screen (IP @, H/W @ and short name).
  - Removal of information balloons.

- **Version 1.1.0 - Build 15**:
  - Moved from ECLIPSE Neon.1 Release (4.6.1)  to IntelliJ IDEA 2016.3.2.

- **Version 1.0.0 - Build 13**:
  - Fix the default delay between 2 scans (inattention error).

- **Version 1.0.0 - Build 12**:
  - Added '.wav' sound file management.

- **Version 1.0.0 - Build 10**:
  - First release.
