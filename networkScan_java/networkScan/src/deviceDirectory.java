//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class deviceDirectory {
	static class host {
		String address;
		String description;
		String sound;
		String getAddr() { return this.address; }
		String getDescription() { return this.description; }
		String getSound() { return this.sound; }
		void setAddr(String address) { this.address = address; }
		void setDescription(String description) { this.description = description; }
		void setSound(String sound) { this.sound = sound; }
	}
	static host[] directory;
	//--------------------------------------------------------------------------
	static void init(String fileName) {
		File f = new File(fileName);
		if (! f.isFile()) {
			directory = new host[0];
			return;
		}
		ArrayList<String> output = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = br.readLine();
			while (line != null) {
				if (line.length() == 0) continue;
				output.add(line);
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] result = new String[output.size()];
		output.toArray(result);
		directory = new host[output.size()];
		for (int k = 0; k < result.length; k++) {
			String line = result[k];
			directory[k] = new host();
			directory[k].setDescription("");
			directory[k].setSound("");
			if (line.contains("	")) {
				String[] aline = line.split("	");
				directory[k].setAddr(aline[0].trim().toLowerCase());
				if (aline.length > 1)
					directory[k].setDescription(aline[1].trim());
				if (aline.length > 2)
					directory[k].setSound(aline[2].trim());
			} else {
				directory[k].setAddr(line.trim().toLowerCase());
			}
		}
	}
	//--------------------------------------------------------------------------
	static int lookfor(String address) {
		address = address.toLowerCase();
		int k = -1;
		if (address.length() != 0) {
			for (int i = 0; i < directory.length; i++) {
				if (directory[i].getAddr().equals(address)) {
					k = i;
					break;
				}
			}
		}
		return k;
	}
}
//==============================================================================
