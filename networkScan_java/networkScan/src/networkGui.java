//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;

class networkGui implements Runnable {
	static Display display = null;
	static Font fontText = null;
	static Font fontConnected = null;
	static Font fontLabel = null;
	static Font fontButton = null;

	private static Shell shell = null;
	private static ScrolledComposite sc = null;
	private static Composite drawingArea = null;
	private static Label status = null;
	private static Button force = null;
	private static Button report = null;
	private static Rectangle[] rects = null;
	private static String[] tooltips = null;
	private static int net_diameter;
	private static int net_coordinates;

	private static final String eol = System.getProperty("line.separator");
	private static final int SHORTBAR_HEIGHT = 20;
	private static final int LONGBAR_HEIGHT = 100;
	private static final int CHAR_HEIGHT = 15;
	private static final int WIDTH_PER_CONNECTION = 80;
	private static final int START_ABSCISSA = WIDTH_PER_CONNECTION / 2 + 40;
	private static final int START_ORDINATE = 5;
	private static final int MAXLENGTH_CONNECTION = 20;
	private static final String UNKNOWN = "Unknown";
	//--------------------------------------------------------------------------
	@Override
	public void run() {
		display = new Display();
		fontText = new Font(display, "Arial", 9, SWT.NORMAL);
		fontButton = new Font(display, "Arial", 10, SWT.BOLD);
		fontLabel = new Font(display, "Arial", 10, SWT.ITALIC);
		fontConnected = new Font(display, "Arial", 10, SWT.BOLD);

		createContent();

		shell.setLocation((display.getBounds().width - shell.getBounds().width) / 2,
				(display.getBounds().height - shell.getBounds().height) / 2);
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
		networkScan.Exit(0);
	}
	//--------------------------------------------------------------------------
	private static void createContent() {
		shell = new Shell(display, SWT.MIN | SWT.MAX | SWT.RESIZE | SWT.CLOSE | SWT.TITLE);
		shell.setLayout(new FormLayout());
		shell.setText(networkScan.sTitle);
		shell.addListener(SWT.Close, event -> networkScan.Exit(0));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		createStatus();
		createButton();
		if (networkScan.circleMode) {
			createCircleDrawingArea();
			displayShell(550);
		} else {
			sc = new ScrolledComposite(shell, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			FormData fd = new FormData();
			fd.top = new FormAttachment(0, 10);
			fd.left = new FormAttachment(0, 5);
			fd.right = new FormAttachment(100, -5);
			fd.bottom = new FormAttachment(status, -10);
			sc.setLayoutData(fd);
			createDrawingArea();
			displayShell(400);
		}
	}
	private static void createStatus() {
		FormData fd1 = new FormData();
		fd1.height = 40;
		fd1.left = new FormAttachment(0, 5);
		fd1.bottom = new FormAttachment(100, -5);
		fd1.right = new FormAttachment(50, -5);

		status = new Label(shell, SWT.LEFT);
		status.setFont(fontLabel);
		status.setText("Initializing...");
		status.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		status.setLayoutData(fd1);
	}
	private static void createButton() {
		FormData fd2 = new FormData();
		fd2.top = new FormAttachment(status, 0, SWT.TOP);
		fd2.bottom = new FormAttachment(status, 0, SWT.BOTTOM);
		fd2.right = new FormAttachment(100, -5);

		Composite buttonbar = new Composite(shell, SWT.NONE);
		buttonbar.setLayoutData(fd2);
		buttonbar.setLayout(new RowLayout());

		force = new Button(buttonbar, SWT.PUSH);
		force.setLayoutData(new RowData(80, 30));
		force.setFont(fontButton);
		force.setText("Force");
		force.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		force.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		force.setEnabled(false);
		force.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				networkScan.scanandupdate(networkScan.forcedScan);
			}
		});
		report = new Button(buttonbar, SWT.PUSH);
		report.setLayoutData(new RowData(80, 30));
		report.setFont(fontButton);
		report.setText("Report");
		report.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		report.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		report.setEnabled(false);
		report.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				networkReport.display();
			}
		});
	}
	//--------------------------------------------------------------------------
	static void update() {
		display.syncExec(() -> {
			Calendar cal= Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			status.setText("Connected devices at " + sdf.format(cal.getTime())
					+ " (x" + networkScan.currentConnections + ")"
					+ eol
					+ "Scan delay="  + networkScan.delay / 1000 + " seconds");
			if (networkScan.currentConnections != 0) {
				switchOffButtons();
				if (networkScan.circleMode)
					displayCircleMode();
				else
					displayRakeMode();
				switchOnButtons();
			} else {
				if (networkScan.circleMode)
					for (int i = 0; i < networkScan.numberOfConnections; i++) {
						if (networkScan.previousRound[i])
							if (networkScan.connectedDevices[i] != null) networkScan.connectedDevices[i].dispose();
					}
			}
			status.update();
		});
	}
	//--------------------------------------------------------------------------
	private static void switchOnButtons() {
		force.setEnabled(true);
		report.setEnabled(true);
	}
	private static void switchOffButtons() {
		force.setEnabled(false);
		report.setEnabled(false);
	}
	//--------------------------------------------------------------------------
	private static void displayShell(int width) {
		int w, h;
		if (networkScan.circleMode) {
			w = width;
			h = 600;
		} else {
			sc.setMinSize(width, 370);
			w = Math.min(width, 1000);
			h = 370;
		}
		shell.pack();
		shell.setSize(w, h);
	}
	//==========================================================================
	//=== Rake Mode ============================================================
	//==========================================================================
	private static void createDrawingArea() {
		drawingArea = new Composite(sc, SWT.NONE);
		drawingArea.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		sc.setContent(drawingArea);
		sc.getVerticalBar().setIncrement(3 * sc.getVerticalBar().getIncrement());
		sc.getHorizontalBar().setIncrement(10);
		sc.setExpandVertical(true);
		sc.setExpandHorizontal(true);
		sc.setAlwaysShowScrollBars(true);
		sc.setMinSize(drawingArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		sc.addListener(SWT.Activate, __ -> sc.setFocus());
		sc.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle r = drawingArea.getClientArea();
				sc.setMinSize(drawingArea.computeSize(r.width, r.height));
				status.update();
			}
		});
	}
	//--------------------------------------------------------------------------
	private static void displayRakeMode() {
		drawGateway(networkScan.currentConnections - 1, networkScan.gateway);
		rects = new Rectangle[networkScan.currentConnections];
		tooltips = new String[networkScan.currentConnections];
		int k = 0;
		for (int i = 0; i < networkScan.numberOfConnections; i++) {
			if (networkScan.connections[i]) {
				boolean unknown, newconnection;
				String[] connection = new String[3];
				String sound;
				connection[2] = networkScan.networkRoot + "." + i;
				connection[1] = networkCmd.getMACAddress(connection[2]);
				if (connection[2].length() != 0 && ! connection[2].equals(networkScan.gateway[2])) {
					int idx = deviceDirectory.lookfor(connection[1]);
					if (idx == -1) {
						unknown = true;
						connection[0] = UNKNOWN;
						sound = "";
					} else {
						unknown = false;
						connection[0] = deviceDirectory.directory[idx].getDescription();
						sound = deviceDirectory.directory[idx].getSound();
					}
					newconnection = ! networkScan.previousRound[i];
					if (newconnection) {
						networkScan.Verbose("New connection " + connection[2]);
						if (sound.length() != 0) networkScan.playSound(sound);
					}
					tooltips[k] = "";
					for (String s : connection) {
						if (tooltips[k].length() != 0) tooltips[k] += "\n";
						tooltips[k] += s;
					}
					rects[k] = drawConnection(k, newconnection, unknown, connection);
					++k;
				}
			} else {
				if (networkScan.previousRound[i]) {
					networkScan.Verbose("Disconnection "
							+ networkScan.networkRoot + "." + i);
				}
			}
		}
		switchOnButtons();
		drawingArea.addListener(SWT.MouseMove, mouseListener);
		drawingArea.addListener(SWT.MouseEnter, mouseListener);
		drawingArea.redraw();

		int w = networkScan.currentConnections * WIDTH_PER_CONNECTION + START_ABSCISSA;
		displayShell(w);
	}
	//--------------------------------------------------------------------------
	private static void drawGateway(int nbConnect, String[] connectionId) {
		drawingArea.dispose();
		createDrawingArea();

		nbConnect = (nbConnect == 0) ? 1 : nbConnect;
		int l = WIDTH_PER_CONNECTION * (nbConnect - 1);
		int x1 = START_ABSCISSA + l / 2;
		drawingArea.addPaintListener(e -> {
			int y = START_ORDINATE;
			e.gc.setFont(fontConnected);
			e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
			for (String s:connectionId) {
				int x2 = x1 - getStringPixelSize(s) / 2;
				if (x2 < 0) x2 = x1;
				e.gc.drawString(s, x2, y);
				e.gc.setFont(fontText);
				y = y + CHAR_HEIGHT;
			}
			e.gc.drawLine(x1, y, x1, y + SHORTBAR_HEIGHT);
			e.gc.drawLine(START_ABSCISSA, y + SHORTBAR_HEIGHT, START_ABSCISSA + l, y + SHORTBAR_HEIGHT);
		});
		drawingArea.layout();
	}
	//--------------------------------------------------------------------------
	private static Rectangle drawConnection(int indexConnection, boolean newConnection, boolean unknownConnection, String[] connectionId) {
		int x = START_ABSCISSA + indexConnection * WIDTH_PER_CONNECTION;
		int y = START_ORDINATE + connectionId.length * CHAR_HEIGHT + SHORTBAR_HEIGHT;
		drawingArea.addPaintListener(e -> {
			if (connectionId[0].length() > MAXLENGTH_CONNECTION)
				connectionId[0] = connectionId[0].substring(0, MAXLENGTH_CONNECTION - 3) + "...";
			int yy = (indexConnection % 2 == 0) ? y + SHORTBAR_HEIGHT :  y + LONGBAR_HEIGHT;
			e.gc.drawLine(x, y, x, yy);
			int xx = x - getLongestStringPixelSize(connectionId) / 2;
			e.gc.setFont(fontConnected);
			if (newConnection)
				e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
			else
				e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
			for (String s : connectionId) {
				e.gc.drawString(s, xx, yy);
				e.gc.setFont(fontText);
				e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				yy = yy + CHAR_HEIGHT;
			}
			if (unknownConnection) {
				final int dim=30;
				Image image = Display.getCurrent().getSystemImage(SWT.ICON_WARNING);
				e.gc.drawImage(image, 0, 0,
						image.getBounds().width,
						image.getBounds().height,
						x - dim / 2,
						connectionId.length * CHAR_HEIGHT + ((indexConnection % 2 == 0) ? y + SHORTBAR_HEIGHT :  y + LONGBAR_HEIGHT) + 5,
						dim, dim);
			}
		});
		return new Rectangle(x - getLongestStringPixelSize(connectionId) / 2,
				(indexConnection % 2 == 0) ? y + SHORTBAR_HEIGHT :  y + LONGBAR_HEIGHT,
				WIDTH_PER_CONNECTION + getLongestStringPixelSize(connectionId) / 2,
				connectionId.length * CHAR_HEIGHT);
	}
	//--------------------------------------------------------------------------
	private static int getLongestStringPixelSize(String[] array) {
		int maxLength = 0;
		String longestString = null;
		for (String s : array)
			if (s.length() > maxLength) {
				maxLength = Math.min(s.length(), MAXLENGTH_CONNECTION);
				longestString = s;
			}
		return getStringPixelSize(longestString);
	}

	private static int getStringPixelSize(String str) {
		Label aa = new Label(shell, SWT.NONE);
		GC gc = new GC(aa);
		Point size = gc.textExtent(str);
		gc.dispose ();
		aa.dispose();
		return size.x;
	}
	//--------------------------------------------------------------------------
	private static final Listener mouseListener = (Event event) -> {
		switch (event.type) {
			case SWT.MouseEnter:
			case SWT.MouseMove:
				for (int i = 0; i < rects.length; i++) {
					if (rects[i] == null) break;
					if (rects[i].contains(event.x, event.y)) {
						drawingArea.setToolTipText(tooltips[i]);
						return;
					}
					drawingArea.setToolTipText(null);
				}
				break;
			default:
				break;
		}
	};
	//==========================================================================
	//=== Circle Mode ==========================================================
	//==========================================================================
	private static void createCircleDrawingArea() {
		FormData fd = new FormData();
		fd.width = 480;
		fd.height = 450;
		fd.top = new FormAttachment(0, 10);
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(100, -5);
		fd.bottom = new FormAttachment(status, -10);

		drawingArea = new Composite(shell, SWT.BORDER);
		drawingArea.setLayoutData(fd);
		drawingArea.addPaintListener(e -> {
			int width = drawingArea.getBounds().width - 80;
			int height = drawingArea.getBounds().height - 80;
			net_diameter = Math.min(width, height);
			net_coordinates = 30;
			e.gc.setLineWidth(1);
			e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
			e.gc.drawOval(net_coordinates, net_coordinates, net_diameter, net_diameter);
		});
	}
	//--------------------------------------------------------------------------
	private static void displayCircleMode() {
		double cx = (net_coordinates + net_diameter) / 2.0;
		double cy = (net_coordinates + net_diameter) / 2.0;
		double radius = net_diameter / 2.0;
		double increment = 360.0 / (double) networkScan.currentConnections;
		int rwidth = 120;  int rheight = 45;
		int k = 0;
		for (int i = 0; i < networkScan.numberOfConnections; i++) {
			String sound;
			String ipAddr = networkScan.networkRoot + "." + i;
			if (networkScan.previousRound[i])
				if (networkScan.connectedDevices[i] != null) networkScan.connectedDevices[i].dispose();
			if (networkScan.connections[i]) {
				int x = ((int) (cx + radius * (Math.cos(k * increment * Math.PI / 180.0)))) - 15;
				int y = ((int) (cy + radius * (Math.sin(k * increment * Math.PI / 180.0))));
				k++;
				networkScan.connectedDevices[i] = new CLabel(drawingArea, SWT.LEFT | SWT.HORIZONTAL);
				networkScan.connectedDevices[i].setFont(fontText);
				//networkScan.connectedDevices[i].setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));

				String addr = networkCmd.getMACAddress(ipAddr);
				int idx = deviceDirectory.lookfor(addr);
				String deviceName = UNKNOWN;
				if (idx == -1) {
					networkScan.connectedDevices[i].setBounds(x, y, 200, rheight + 10);
					networkScan.connectedDevices[i].setImage(display.getSystemImage(SWT.ICON_WARNING));
					sound = "";
				} else {
					networkScan.connectedDevices[i].setBounds(x, y, rwidth, rheight);
					deviceName = deviceDirectory.directory[idx].getDescription();
					sound = deviceDirectory.directory[idx].getSound();
				}
				String text = deviceName + "\n" + addr + "\n" + ipAddr;
				networkScan.connectedDevices[i].setText(text);
				networkScan.connectedDevices[i].setToolTipText(text);

				if (networkScan.previousRound[i]) {
					networkScan.connectedDevices[i].setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				} else {
					networkScan.Verbose("New connection " + ipAddr);
					networkScan.connectedDevices[i].setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
					if (sound.length() != 0) networkScan.playSound(sound);
				}
			} else {
				if (networkScan.previousRound[i])
					networkScan.Verbose("Disconnection " + ipAddr);
			}
		}
		drawingArea.redraw();
	}
}
//==============================================================================
