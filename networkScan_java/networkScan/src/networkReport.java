//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

class networkReport {
	private static Font fontText = null;
	private static final String[] columnNames = { "#  ", "IP Address   ", "Physical H/W Address", "Short Description", "Sound"};
	private static final int[] columnWidths	= { 30, 100, 150, 200, 200};
	//--------------------------------------------------------------------------
	static void display() {
		fontText = new Font(networkGui.display, "Arial", 10, SWT.NORMAL);

		Shell shell = new Shell(Display.getCurrent(), SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setText(networkScan.sTitle);
		shell.setLayout(new GridLayout(1, true));

		createReport(shell);

		shell.setSize(700, 300);
		// Center the shell on the primary monitor
		Monitor primary = Display.getCurrent().getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = shell.getBounds ();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation (x, y);

		shell.open();
		while (!shell.isDisposed()) {
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
		}
	}
	//--------------------------------------------------------------------------
	private static void createReport(Composite parent) {
		Table viewer = new Table(parent, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION | SWT.NO_FOCUS | SWT.H_SCROLL | SWT.V_SCROLL);

		GridData viewergd = new GridData();
		viewergd.grabExcessHorizontalSpace = true;
		viewergd.grabExcessVerticalSpace = true;
		viewergd.widthHint = viewergd.minimumWidth = 10;
		viewergd.horizontalAlignment = GridData.FILL;
		viewergd.horizontalIndent = 0;
		viewergd.horizontalSpan = 1;
		viewergd.verticalAlignment = GridData.FILL;
		viewergd.verticalIndent = 0;
		viewergd.verticalSpan = 0;

		viewer.setLayoutData(viewergd);
		viewer.setLinesVisible(true);
		viewer.setHeaderVisible(true);
		viewer.setFont(fontText);
		for (int i = 0; i < columnNames.length; i++) {
			TableColumn column = new TableColumn(viewer, SWT.NONE);
			column.setText(columnNames[i]);
		}
		for (int i = 0; i < columnNames.length; i++) {
			viewer.getColumn(i).pack();
		}
		for (int i = 0; i < columnWidths.length; i++) {
			viewer.getColumn(i).setWidth(columnWidths[i]);
		}

		int line = 0;
		for (int i = 0; i < networkScan.numberOfConnections; i++) {
			if (networkScan.connections[i]) {
				String addr = networkCmd.getMACAddress(networkScan.networkRoot + "." + i);
				int k = deviceDirectory.lookfor(addr);
				TableItem item = new TableItem(viewer, SWT.NONE);
				item.setText(0, "" + line);
				item.setText(1, networkScan.networkRoot + "." + i);
				item.setText(2, addr);
				if (k == -1) {
					item.setText(3, "Unknown");
					item.setText(4, "");
					viewer.getItem(line).setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
				} else {
					item.setText(3, deviceDirectory.directory[k].getDescription());
					item.setText(4, deviceDirectory.directory[k].getSound());
				}

				line++;
			}
		}
	}
}
//==============================================================================
