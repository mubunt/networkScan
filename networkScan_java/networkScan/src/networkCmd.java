//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

class networkCmd {
	private static final String cmd_ping = "ping";
	private static final String cmd_nmap = "nmap -n -sP %s.0/24";
	private static final String cmdLinux_arp = "arp %s";
	private static final String cmdWindows_arp = "arp -a %s";
	private static final String cmdLinux_route = "route -n";
	private static final String cmdWindows_route = "route -4 PRINT";
	//--------------------------------------------------------------------------
	static void scan(boolean scanType) {
		for (int i = 0; i < networkScan.numberOfConnections; i++) {
			networkScan.previousRound[i] = networkScan.connections[i];
			networkScan.connections[i] = false;
		}
		networkScan.currentConnections = 0;
		deviceDiscovery(scanType);
	}

	private static void deviceDiscovery(boolean scanType) {
		if (scanType == networkScan.forcedScan)
			networkScan.Verbose("Forced network scanning");
		else
			networkScan.Verbose("Scheduled network scanning");
		String[] output = executeCommand(String.format(cmd_nmap, networkScan.networkRoot));
		// Starting Nmap 7.40 ( https://nmap.org ) at 2017-01-11 17:04 Europe de l?Ouest
		// Nmap scan report for 192.168.0.1
		// Host is up (0.015s latency).
		// MAC Address: F4:CA:E5:49:0D:2C (Freebox SAS)
		// Nmap scan report for 192.168.0.13
		// Host is up (0.00s latency).
		// MAC Address: 00:24:D4:71:72:A4 (Freebox SAS)
		// ...
		// Nmap scan report for 192.168.0.22
		// Host is up.
		// Nmap done: 256 IP addresses (8 hosts up) scanned in 8.11 seconds
		String prefix = "Nmap scan report for " + networkScan.networkRoot + ".";
		for (String line : output) {
			if (line.length() == 0) continue;
			if (line.startsWith(prefix)) {
				int i = Integer.parseInt(line.substring(prefix.length()));
				networkScan.connections[i] = true;
				networkScan.currentConnections++;
				networkScan.Verbose("Discover device" + networkScan.networkRoot + "." + i);
			}
		}
	}
	//--------------------------------------------------------------------------
	static String getGateWayIPAddress() {
		String myGatewayAddress = "";
		String command = cmdLinux_route;
		if (networkScan.isWindows)
			command = cmdWindows_route;
		String[] output = executeCommand(command);
		final String addrnull = "0.0.0.0";
		for (String line : output) {
			if (line.length() == 0) continue;
			if (line.trim().startsWith(addrnull)) {
				if (networkScan.isWindows) {
					// >route -4 PRINT
					// ===========================================================================
					// Liste d'Interfaces
					// 19...7c 05 07 72 08 b2 ......Contrôleur Realtek PCIe GBE Family
					// 5...02 00 4c 4f 4f 50 ......Npcap Loopback Adapter
					// .....
					// ===========================================================================
					//
					// IPv4 Table de routage
					// ===========================================================================
					// Itinéraires actifs :
					// Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
					// 0.0.0.0          0.0.0.0      192.168.0.1     192.168.0.22     55
					// 127.0.0.0        255.0.0.0         On-link         127.0.0.1    331
					// 127.0.0.1  255.255.255.255         On-link         127.0.0.1    331
					// .....
					myGatewayAddress = line.trim().replace(addrnull, "").trim().replace(addrnull, "").trim();
				} else {
					// $ route -n
					// Table de routage IP du noyau
					// Destination Passerelle      Genmask         Indic Metric Ref    Use Iface
					// 0.0.0.0         192.168.0.1     0.0.0.0         UG    100    0        0 enp7s0
					// 169.254.0.0     0.0.0.0         255.255.0.0     U     1000   0        0 enp7s0
					// 192.168.0.0     0.0.0.0         255.255.255.0   U     100    0        0 enp7s0
					// $
					myGatewayAddress = line.trim().replace(addrnull, "").trim();
				}
				myGatewayAddress = myGatewayAddress.substring(0, myGatewayAddress.indexOf(" "));
				break;
			}
		}
		networkScan.Verbose("The Gateway IP address is " + myGatewayAddress);
		return myGatewayAddress;
	}
	//--------------------------------------------------------------------------
	static String getMACAddress(String ipAddr) {
		if (networkScan.myIPAddress.equals(ipAddr)) {
			networkScan.Verbose("MAC Address of " + ipAddr + " is " + networkScan.myMACAddr);
			return(networkScan.myMACAddr);
		}
		String addr = "";
		if (ping3(ipAddr)) {
			if (networkScan.isWindows) {
				String[] output = executeCommand(String.format(cmdWindows_arp, ipAddr));
				if (output.length >= 3) {
					String line = output[2].trim();
					line = line.replace(ipAddr, "").trim();
					addr = line.substring(0, line.indexOf(" ")).replaceAll("-", ":");
				}
			} else {
				String[] output = executeCommand(String.format(cmdLinux_arp, ipAddr));
				String prefix2 = "_gateway";
				String prefix3 = ipAddr + " (" + ipAddr + ") --";
				for (String line : output) {
					if (line.length() == 0) continue;
					networkScan.Verbose("arp " + ipAddr + ": " + line);
					if (line.startsWith(prefix3)) {
						// 192.168.0.n (192.168.0.n) -- no entry
						//addr = networkScan.myMACAddr;	// Either my @ (=> my MAC @) or another one (=> "")
						addr = "";
						break;
					}
					if (line.startsWith(ipAddr)) {
						// 192.168.0.40             ether   d8:eb:97:1b:ea:8d   C                     enp7s0
						addr = line.replace(ipAddr, "").trim().replace("ether", "").trim();
						addr = addr.substring(0, addr.indexOf(" "));
						break;
					}
					if (line.startsWith(prefix2)) {
						// gateway             ether   d8:eb:97:1b:ea:8d   C enp7s0
						addr = line.replace(prefix2, "").trim().replace("ether", "").trim();
						addr = addr.substring(0, addr.indexOf(" "));
						break;
					}
				}
			}
		}
		networkScan.Verbose("MAC Address of " + ipAddr + " is " + addr);
		return(addr);
	}
	//--------------------------------------------------------------------------
	private static String[] executeCommand(String command) {
		ArrayList<String> output = new ArrayList<>();
		try {
			Process p = Runtime.getRuntime().exec(command);
			if (! networkScan.isWindows) p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.length() != 0) { output.add(line); }
			}
		} catch (Exception e) {
			networkScan.Error("Execution error: " + e.getMessage());
		}
		String[] result = new String[output.size()];
		output.toArray(result);
		return result;
	}
	//--------------------------------------------------------------------------
	private static boolean ping3(String host) {
		ProcessBuilder processBuilder = new ProcessBuilder(cmd_ping, networkScan.isWindows ? "-n" : "-c", "1", host);
		Process proc;
		try {
			proc = processBuilder.start();
			int returnVal = proc.waitFor();
			return returnVal == 0;
		} catch (IOException|InterruptedException e) {
			return false;
		}
	}
}
//==============================================================================
