//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.eclipse.swt.custom.CLabel;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;

@SuppressWarnings("InfiniteLoopStatement")
public class networkScan {
	private static final String jarFile = "networkScan.jar";
	private static final String sVerbose = "[VERBOSE] ";
	private static final String sError = "!!! ERROR !!! ";
	private static final String sExit = "EXIT...";
	private static final String deviceDirectory_default = "HWAddress.directory";
	private static final int delay_default = 60000;	// milliseconds = 60 seconds
	private static final boolean scheduledScan = false;
	private static final Object lock = new Object();
	private static final String slash = System.getProperty("file.separator");
	private static final String os = System.getProperty("os.name");
	private static final String intelliJobjpath = "networkScan_java/networkScan/out/production/networkScan/";
	private static String InstallRootDir = null;
	private static boolean verboseMode = false;

	static final String sTitle = "Network Scanner";
	static final int numberOfConnections = 256;		// max number of connection per network
	static String networkRoot = "192.168.0";
	static String myIPAddress = null;
	static String myMACAddr = null;
	static boolean circleMode = false;
	static int currentConnections = 0;
	static final boolean forcedScan	= true;
	static boolean isWindows = false;
	static String[] gateway = new String[3];
	static int delay = delay_default;
	static boolean[] connections = new boolean[numberOfConnections];
	static boolean[] previousRound = new boolean[numberOfConnections];
	static CLabel[] connectedDevices = new CLabel[numberOfConnections];
	//--------------------------------------------------------------------------
	public static void main(String[] args) {
		InstallRootDir = networkScan.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		InstallRootDir = InstallRootDir.replace(jarFile, "");
		if (InstallRootDir.length() > intelliJobjpath.length()) {
			String s = InstallRootDir.substring(InstallRootDir.length() - intelliJobjpath.length());
			if (s.equals(intelliJobjpath)) {
				InstallRootDir = InstallRootDir.substring(0, InstallRootDir.length() - intelliJobjpath.length() - 1)
						+ slash + "networkScan_bin" + slash;
			}
		}
		// Current platform
		if (os.startsWith("Windows")) isWindows = true;
		//----------------------------------------------------------------------
		// OPTIONS PROCESSING
		String deviceDirectoryFile = InstallRootDir + deviceDirectory_default;
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",       false, "print this message and exit");
		options.addOption("V", "version",    false, "print the version information and exit");
		options.addOption("v", "verbose",    false, "verbose mode");
		options.addOption("C", "circle",     false, "Representation of the network as a circle");
		options.addOption("d", "directory",  true,  "Physical hardware address directory. Default is '" + deviceDirectory_default + "'");
		options.addOption("D", "delay",   	 true, 	"Delay in seconds between 2 scans. Default is " + delay_default + " seconds");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				Exit(0);
			}
			// Option: Version
			if (line.hasOption("version")) {
				System.out.println(jarFile +
				 	"	Version: " + networkBuildInfo.getVersion() +
					" - Build: " + networkBuildInfo.getNumber() +
					" - Date: " + networkBuildInfo.getDate());
				Exit(0);
			}
			// Option: Verbose
			if (line.hasOption("verbose")) {
				verboseMode = true;
			}
			// Option: Local
			if (line.hasOption("circle")) {
				circleMode = true;
			}
			// Option: Directory
			if (line.hasOption("directory")) {
				deviceDirectoryFile = line.getOptionValue("directory");
				File f = new File(deviceDirectoryFile);
				if (! f.isFile()) {
					Error("File " + deviceDirectoryFile + " does not exist");
				}
			}
			// Option: Delay
			if (line.hasOption("delay")) {
				String sdelay = line.getOptionValue("delay");
				try {
					delay = Integer.parseInt(sdelay) * 1000;	// Seconds to milliseconds
				} catch (NumberFormatException nfe) {
					Error("'" + sdelay + "' is not a valid number");
				}
			}
		} catch (ParseException exp) { Error("Parsing failed - " + exp.getMessage()); }
		//----------------------------------------------------------------------
		// MY HOST IP AND PHYSICAL ADDRESSES
		myIPAddress = getMyLANAddress();
		if ("127.0.1.1".equals(myIPAddress) || !validIP(myIPAddress)) {
			Error("It seems that there is no TCP/IP connection....");
		}
		//----------------------------------------------------------------------
		// MY MAC ADDRESS
		myMACAddr = getMyMACAddress(myIPAddress);
		//----------------------------------------------------------------------
		// GATEWAY AND NETWORK ADDRESSES
		String gatewayAddress = networkCmd.getGateWayIPAddress();
		networkRoot = gatewayAddress.substring(0, gatewayAddress.lastIndexOf("."));
		//----------------------------------------------------------------------
		// DEVICE DIRECTORY PROCESSING
		deviceDirectory.init(deviceDirectoryFile);
		//----------------------------------------------------------------------
		if (verboseMode) {
			System.out.println("Context:");
			System.out.println("	Gateway ............... " + gatewayAddress);
			System.out.println("	Network ............... " + networkRoot);
			System.out.println("	My local IP address ... " + myIPAddress);
			System.out.println("	My MAC address ........ " + myMACAddr);
			System.out.println("	Directory ............. " + deviceDirectoryFile);
			System.out.println("	Scan Delay ............ " + delay + " milliseconds");
		}
		//----------------------------------------------------------------------
		// GO ON....
		gateway[2] = gatewayAddress;
		gateway[1] = networkCmd.getMACAddress(gatewayAddress);
		int idx = deviceDirectory.lookfor(gateway[1]);
		gateway[0] = (idx == -1) ? "Unknown" : deviceDirectory.directory[idx].getDescription();

		// For circle representation
		for (int i = 0; i < numberOfConnections; i++)
			connections[i] = false;

		final networkGui gui = new networkGui();
		Thread t = new Thread(gui);
		t.start();
		try { Thread.sleep(1000); } catch (InterruptedException e) { Error("Interrupted Exception: '" + e.getMessage()); }
		do {
			scanandupdate(scheduledScan);
			try { Thread.sleep(delay); } catch (InterruptedException e) { Error("Interrupted Exception: '" + e.getMessage()); }
		} while (true);
	}
	//--------------------------------------------------------------------------
	static void scanandupdate(boolean scanType) {
		synchronized(lock) {
			networkCmd.scan(scanType);
			networkGui.update();
		}
	}
	//--------------------------------------------------------------------------
	/**
	 * Returns an <code>InetAddress</code> object encapsulating what is most likely the machine's LAN IP address.
	 * <p/>
	 * This method is intended for use as a replacement of JDK method <code>InetAddress.getLocalHost</code>, because
	 * that method is ambiguous on Linux systems. Linux systems enumerate the loopback network interface the same
	 * way as regular LAN network interfaces, but the JDK <code>InetAddress.getLocalHost</code> method does not
	 * specify the algorithm used to select the address returned under such circumstances, and will often return the
	 * loopback address, which is not valid for network communication. Details
	 * <a href="http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4665037">here</a>.
	 * <p/>
	 * This method will scan all IP addresses on all network interfaces on the host machine to determine the IP address
	 * most likely to be the machine's LAN address. If the machine has multiple IP addresses, this method will prefer
	 * a site-local IP address (e.g. 192.168.x.x or 10.10.x.x, usually IPv4) if the machine has one (and will return the
	 * first site-local address if the machine has more than one), but if the machine does not hold a site-local
	 * address, this method will return simply the first non-loopback address found (IPv4 or IPv6).
	 * <p/>
	 * If this method cannot find a non-loopback address using this selection algorithm, it will fall back to
	 * calling and returning the result of JDK method <code>InetAddress.getLocalHost</code>.
	 * <p/>
	 */
	private static String getMyLANAddress() {
		try {
			InetAddress candidateAddress = null;
			// Iterate all NICs (network interface cards)...
			for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements(); ) {
				NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
				// Iterate all IP addresses assigned to each card...
				for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements(); ) {
					InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
					if (!inetAddr.isLoopbackAddress()) {
						if (inetAddr.isSiteLocalAddress()) {
							// Found non-loopback site-local address. Return it immediately...
							String s = inetAddr.toString();
							if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
							return s;
						} else if (candidateAddress == null) {
							// Found non-loopback address, but not necessarily site-local.
							// Store it as a candidate to be returned if site-local address is not subsequently found...
							candidateAddress = inetAddr;
							// Note that we don't repeatedly assign non-loopback non-site-local addresses as candidates,
							// only the first. For subsequent iterations, candidate will be non-null.
						}
					}
				}
			}
			if (candidateAddress != null) {
				// We did not find a site-local address, but we found some other non-loopback address.
				// Server might have a non-site-local address assigned to its NIC (or it might be running
				// IPv6 which deprecates the "site-local" concept).
				// Return this non-loopback candidate address...
				String s = candidateAddress.toString();
				if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
				return s;
			}
			// At this point, we did not find a non-loopback address.
			// Fall back to returning whatever InetAddress.getLocalHost() returns...
			InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
			if (jdkSuppliedAddress == null) {
				Error("The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
			}
			String s = jdkSuppliedAddress.toString();
			if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
			return s;
		} catch (Exception e) {
			Error("Unknown Host Exception: '" + e.getMessage());
		}
		return null;
	}
	//--------------------------------------------------------------------------
	private static String getMyMACAddress(String ipAddr) {
		byte[] mac = new byte[0];
		try {
			NetworkInterface network = NetworkInterface.getByInetAddress(InetAddress.getByName(ipAddr));
			mac = network.getHardwareAddress();
		} catch (SocketException e) {
			Error("Socket Error: " + e.getMessage());
		} catch (UnknownHostException e) {
			Error("Unknown Host: " + e.getMessage());
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
		}
		return sb.toString().toLowerCase().replaceAll("-", ":");
	}
	//--------------------------------------------------------------------------
	static void Verbose(String s) {
		if (verboseMode) {
			Calendar cal= Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			System.out.println(sVerbose + sdf.format(cal.getTime()) + " " + s);
		}
	}
	//--------------------------------------------------------------------------
	static void Error(String s) {
		System.err.println(sError + s);
		Exit(-1);
	}
	//--------------------------------------------------------------------------
	static void Exit(int value) {
		if (networkGui.fontText != null) networkGui.fontText.dispose();
		if (networkGui.fontConnected != null) networkGui.fontConnected.dispose();
		if (networkGui.fontLabel != null) networkGui.fontLabel.dispose();
		if (networkGui.fontButton != null) networkGui.fontButton.dispose();
		if (networkGui.display != null) networkGui.display.dispose();
		if (value == 0) Verbose(sExit);
		System.exit(value);
	}
	//--------------------------------------------------------------------------
	private static boolean validIP(String ip) {
		try {
			if (ip == null || ip.isEmpty()) { return false; }
			String[] parts = ip.split( "\\." );
			if (parts.length != 4) { return false; }
			for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) { return false; }
			}
			return !ip.endsWith(".");
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	//--------------------------------------------------------------------------
	static void playSound(String fsound) {
		String soundfile = fsound;
		if (! fsound.startsWith("/")) soundfile = InstallRootDir + soundfile;
		File soundFile = new File(soundfile);
		AudioInputStream sound;
		Clip clip = null;
		try {
			sound = AudioSystem.getAudioInputStream(soundFile);
			DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
			clip = (Clip) AudioSystem.getLine(info);
			clip.open(sound);
		} catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
			Error("Cannot manage the sound '" + soundfile + "': " + e.getMessage());
		}
		if (clip != null) clip.start();
	}
}
//==============================================================================
