//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef LINUX
#include <sys/wait.h>
#else
#include <windows.h>
#include <winbase.h>
#endif
//------------------------------------------------------------------------------
// MACROS DEFINITIONS TO CUSTOMIZE THE PROGRAM
//------------------------------------------------------------------------------
#define BINDIR					"/networkScan_bin/"
#define NETWORKSCAN 			"networkScan.jar"
#define HEADER					"networkScan_cmdline.h"
#define PARSER 					cmdline_parser_networkScan
#define FREE 					cmdline_parser_networkScan_free
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include HEADER
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define DISPLAY 				"DISPLAY"
#define JAVA 					"java"
#ifdef LINUX
#define ProcessPseudoFileSystem	"/proc/self/exe"
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define error(fmt, ...) 		do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define EXIST(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static char jarpath[PATH_MAX];
static struct stat locstat, *ptlocstat;        // stat variables for EXIST*
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static bool testJARfile(char *jarFile) {
	char jarfile[PATH_MAX];
	snprintf(jarfile, sizeof(jarfile), "%s%s",jarpath, jarFile);
	if (! EXIST(jarfile)) {
		error("WRONG INSTALLATION - Jar file '%s' does not exist.", jarfile);
		return false;
	}
	return true;
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Return code (EXIT_SUCCESS - Normal program stop / EXIT_FAILURE - Program stop on error)
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	char jarfile[PATH_MAX];
	char command[4096 * 4];
	char *pt;
	unsigned int i;
	int j, retval = 0;
	size_t k;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	//---- Parameter checking and setting --------------------------------------
#ifdef LINUX
	//----- Get real path of this executable to compute the path JARS files
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	if (readlink(ProcessPseudoFileSystem, jarpath, sizeof(jarpath)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
#else
	if (0 == GetModuleFileName(NULL, jarpath, sizeof(jarpath))) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
#endif
	dirname(jarpath);
	strcat(jarpath, BINDIR);
	//---- Check consistency of the installation
	if (! EXISTDIR(jarpath)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", jarpath);
		return EXIT_FAILURE;
	}
	if (! testJARfile((char *)NETWORKSCAN)) return EXIT_FAILURE;
	if (! testJARfile((char *)COMMONSCLI)) return EXIT_FAILURE;
	if (! testJARfile((char *)SWT)) return EXIT_FAILURE;
	//---- Check consistency of the environment
	if (NULL == getenv(DISPLAY)) {
		error("%s", "WRONG ENVIRONMENT - DISPLAY not initialized.");
		return EXIT_FAILURE;
	}
	//---- Parameter checking and setting --------------------------------------
	if (PARSER(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Run Java
	sprintf(command, "%s -cp %s -jar %s%s", JAVA, jarpath, jarpath, NETWORKSCAN);
	for (j = 1; j < argc; j++) {
		strcat(command, " ");
		pt = argv[j];
		k = strlen(command);
		while (*pt != '\0') {
			if (*pt == ' ') command[k++] = '\\';
			command[k] = *pt;
			command[++k] = '\0';
			++pt;
		}
	}
	retval = system(command);
	//---- End -------------------------------------------------------------------
	FREE(&args_info);
	return(retval);
}
//------------------------------------------------------------------------------
